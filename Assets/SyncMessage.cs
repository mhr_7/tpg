﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncMessage : MonoBehaviour
{
    public Transform contentPanel;
    public MessData m_data;
    public SimpleObjectPool m_pool;

    public CobaScroll m_scroll;

    // Start is called before the first frame update
    void Start()
    {
        m_data = GameObject.Find("GameLogs/MessageManager").gameObject.GetComponent<MessData>();
        Refresh();
    }

    void Refresh()
    {
        ClearMessage();
        AddButtons();
    }

    private void AddButtons()
    {
        int num = m_data.getList("acc").Count;
        for (int i = 0; i < num; i++)
        {
            //MessageItem item = itemList[i];
            MessageItem item = m_data.getOne("acc", i);
            GameObject newButton = m_pool.GetObject();
            newButton.transform.SetParent(contentPanel);

            MessageHolderSmall sampleButton = newButton.GetComponent<MessageHolderSmall>();
            sampleButton.Setup(item, this, "Sell");
        }

        num = m_data.getList("all").Count;
        for (int i = 0; i < num; i++)
        {
            //MessageItem item = itemList[i];
            MessageItem item = m_data.getOne("all", i);
            GameObject newButton = m_pool.GetObject();
            newButton.transform.SetParent(contentPanel);

            MessageHolderSmall sampleButton = newButton.GetComponent<MessageHolderSmall>();
            sampleButton.Setup(item, this, "Unread");
        }

        contentPanel.gameObject.SetActive(false);
    }

    private void ClearMessage()
    {
        while (contentPanel.childCount > 0)
        {
            GameObject toRemove = contentPanel.GetChild(0).gameObject;
            m_pool.ReturnObject(toRemove);
        }
    }

    public void GoToDetail(MessageItem m)
    {
        m_scroll.goToDescription(m);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
