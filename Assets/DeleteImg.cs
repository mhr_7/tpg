﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class DeleteImg : MonoBehaviour
{
    public int id;
    public string fileName;
    public Sprite imgSprite;

    public void SetUp(int i, string name, Sprite sp)
    {
        id = i;
        fileName = name;
        imgSprite = sp;
    }

    public void DoDeleteImg()
    {
        File.Delete(fileName);
        Debug.Log(fileName + " is Deleted");
        gameObject.GetComponent<Image>().sprite = null;
    }
}
