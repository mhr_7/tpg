﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inventory : MonoBehaviour
{
    public List<GameObject> inven = new List<GameObject>();

    public void invObj(GameObject g){
        inven.Add(g);
    }

    public GameObject retObj(int it){
      GameObject temp;
      temp = inven[it];
      inven.Remove(inven[it]);
      return temp;
    }
}
