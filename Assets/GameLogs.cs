﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogs : MonoBehaviour
{
    public static GameLogs instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
