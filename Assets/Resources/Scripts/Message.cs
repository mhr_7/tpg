using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

public class Message : MonoBehaviour {

    public MessageData data = new MessageData();

    public string name = "message";
    public string city;
    //public Sprite profile;
    public int dueDate;
    public int qty;
    public string orderType;
    public float price;

    public void StoreData(){
        data.name = name;
        data.city = city;
        data.orderType = orderType;
        data.dueDate = dueDate;
        data.price = price;
        data.qty = qty;
    }

    public void LoadData(){
      name = data.name;
      city = data.city;
      orderType = data.orderType;
      dueDate = data.dueDate;
      price = data.price;
      qty = data.qty;
    }
}

public class MessageData
{
  //public Sprite profile;

    [XmlAttribute("Name")]
    public string name;

    [XmlElement("City")]
    public string city;

    [XmlElement("OrderType")]
    public string orderType;

    [XmlElement("Price")]
    public float price;

    [XmlElement("DueDate")]
    public int dueDate;

    [XmlElement("Qty")]
    public int qty;
}
