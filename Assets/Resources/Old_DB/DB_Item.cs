﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

public class DataBse : MonoBehaviour{
	public DB_Item data = new DB_Item ();
}

public class DB_Item{
	[XmlEnum("id_item")]
	public string id_item;

	[XmlAttribute("item_name")]
	public string item_name;

	[XmlElement("damage")]
	public string damage;

	[XmlElement("durability")]
	public string durability;
		
}
	