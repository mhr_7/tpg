﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("ItemCollection")]
public class DB_ItemContainer{

	[XmlArray("DB_Items")]
	[XmlArrayItem("DB_Item")]

	public List<DB_Item> DB_items = new List<DB_Item>();

	public static DB_ItemContainer Load (string path){
		TextAsset _xml = Resources.Load<TextAsset> (path);

		XmlSerializer serializer = new XmlSerializer (typeof(DB_ItemContainer));

		StringReader reader = new StringReader (_xml.text);

		DB_ItemContainer DB_Items = serializer.Deserialize (reader) as DB_ItemContainer;

		reader.Close();

		return DB_Items;
	}
}
