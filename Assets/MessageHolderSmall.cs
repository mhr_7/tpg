﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MessageHolderSmall : MonoBehaviour
{
    public Text status, deadline;
    public Button button;

    private SyncMessage list;
    private MessageItem mItem;
    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(HandleClick);
    }

    public void Setup(MessageItem meItem, SyncMessage current_list, string stat)
    {
        list = current_list;

        status.text = stat;
        deadline.text = meItem.dueDate.ToString();

        mItem = meItem;
    }

    public void HandleClick()
    {
        list.GoToDetail(mItem);
    }
}
