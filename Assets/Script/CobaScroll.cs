﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CobaScroll : MonoBehaviour {

	public GameObject[] panel;
	//public 
	public ScrollRect myScrollRect;
	public RectTransform[] scrollableContent;
	public int content_th, temp_content;
	public bool isOutside;

    public SimpleObjectPool imgpool;
    public Transform img_content;

    string[] files = null;
    int whichScreenShotIsShown = 0;

    private RectTransform rt;
	private float speed, step;

    public Text DescDueDate, DescOrder;
    private MessageItem activeMessage;
	// Use this for initialization
	void Start () {
		speed = 0.1f;
		isOutside = true;

		rt = panel[0].GetComponent<RectTransform>();

		content_th = 0;
		scrollableContent [0].gameObject.SetActive (true);
		myScrollRect.content = scrollableContent [0];
		temp_content = 0;
	}
	
	// Update is called once per frame
	void Update () {
		step = speed * Time.deltaTime;

		if (content_th != temp_content) {
			scrollableContent [content_th].gameObject.SetActive (true);
			myScrollRect.content = scrollableContent [content_th];
			scrollableContent [temp_content].gameObject.SetActive (false);
			temp_content = content_th;
		} 
	}

	public void justMove(){
		if (isOutside) {
			rt.position = Vector3.Lerp (panel [1].transform.position, panel [2].transform.position, step);
			isOutside = false;
		} else {
			rt.position = Vector3.Lerp (panel [2].transform.position, panel [1].transform.position, step);
			isOutside = true;
		}
	}

	public void goToMailBox(){
		content_th = 1;
	}
	public void goToDescription(MessageItem mItem){
		content_th = 2;

        DescDueDate.text = "Due to: " + mItem.dueDate.ToString();
        DescOrder.text = mItem.orderType + " = " + mItem.qty.ToString();
        activeMessage = mItem;
	}

    public void sendOrder()
    {
        activeMessage.status = 1;
        goToMailBox();
    }
    
    public void goToMainMenu()
    {
        content_th = 0;
    }

    public void goToOthers()
    {

    }

    public void goToPhoto()
    {
        content_th = 3;
    }

    public void TakeAShot()
    {
        StartCoroutine("CaptureIt");
    }

    IEnumerator CaptureIt()
    {
        string timeStamp = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss");
        string fileName = "olshopSim" + timeStamp + ".png";
        string pathToSave = Application.persistentDataPath + "/" + fileName;
        print("save to: " + pathToSave);
        ScreenCapture.CaptureScreenshot(pathToSave);
        yield return new WaitForEndOfFrame();
        //Instantiate(blink, new Vector2(0f, 0f), Quaternion.identity);
    }

    public void refresh()
    {
        files = Directory.GetFiles(Application.persistentDataPath + "/", "*.png");
        //print("File Name : " + files);
        if(img_content.childCount > 1 )
            removeImage();
        if (files.Length > 0)
        {
            addImage();
            //GetPictureAndShowIt();
        }
    }

    public void removeImage()
    {
        //print("number of child: "+ img_content.getChild(1).gameObject.name);
        while (img_content.childCount > 1)
        {
            GameObject toRemove = img_content.GetChild(0).gameObject;
            imgpool.ReturnObject(toRemove);
        }
    }

    public void addImage()
    {
        for(int i=0; i<files.Length; i++)
        {
            string pathToFile = files[i];
            Texture2D texture = GetScreenshotImage(pathToFile);
            Sprite sp = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                new Vector2(0.5f, 0.5f));

            GameObject newButton = imgpool.GetObject();
            newButton.transform.SetParent(img_content);

            newButton.GetComponent<Image>().sprite = sp;

            newButton.GetComponent<DeleteImg>().SetUp(i, pathToFile, sp);
        }
    }

    Texture2D GetScreenshotImage(string filePath)
    {
        Texture2D texture = null;
        byte[] fileBytes;
        if (File.Exists(filePath))
        {
            fileBytes = File.ReadAllBytes(filePath);
            texture = new Texture2D(2, 2, TextureFormat.RGB24, false);
            texture.LoadImage(fileBytes);
        }
        return texture;
    }

    public void goToScene(string name)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(name);
    }
}
