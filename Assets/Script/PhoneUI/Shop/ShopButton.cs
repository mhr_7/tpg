﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopButton : MonoBehaviour {

	public Text nameTag;
	public Text priceTag;
	public Image potoTag;
	public Image soldOut;
	public Button butt, buyButton;
	public bool purchaced;

	public ShopItem sItem;
	public shopScrollList sScrollList;
	// Use this for initialization
	void Start () {
		butt.onClick.AddListener (clickHandle);
	}
	
	public void Setup(ShopItem curentItem, shopScrollList currentScrollList){
		sItem = curentItem;
		nameTag.text = sItem.names;
		potoTag.sprite = sItem.potos;
		priceTag.text = "Price: "+sItem.price.ToString ();

        refreshSoldOut();

		sScrollList = currentScrollList;
	}

    public void refreshSoldOut()
    {
        if (sItem.purchaced)
        {
            soldOut.gameObject.SetActive(true);
        }
        else
        {
            soldOut.gameObject.SetActive(false);
        }
    }

    public void buy()
    {

    }

	public void clickHandle(){
		sScrollList.goDetail (sItem);
	}
}
