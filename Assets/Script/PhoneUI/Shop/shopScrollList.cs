﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class ShopItem
{
	public string names;
	public string effect;
	public string desc;
	public Sprite potos;
	public float price;
	public bool purchaced;
}

public class shopScrollList : MonoBehaviour {

	public List<ShopItem> shopList;
	public Transform contentPanel;

    public ShopDB sShop;
	//public ShopScrollList onlineShop;
	//public PlayerStat player;
	public Text myGoldDisplay;
	public SimpleObjectPool buttonObjectPool;
	public int time = 0, itemItt;

	public Text nameTag;
	public Text descTag;
	public Text priceTag;
	public Image pictTag;

	public float gold = 20f;
	public PlayerStat player_stat;

	// Use this for initialization
	void Start () {
        addToList();
		RefreshDisplay ();
		//gold = player_stat.money;
	}

    public void addToList()
    {
        for(int i=0; i<sShop.names.Length; i++)
        {
            ShopItem item = new ShopItem();
            item.names = sShop.names[i];
            item.effect = sShop.effect[i];
            item.desc = sShop.desc[i];
            item.potos = sShop.potos[i];
            item.price = sShop.price[i];
            item.purchaced = sShop.purchaced[i];
            shopList.Add(item);
        }
    }
	
	public void RefreshDisplay()
	{
		myGoldDisplay.text = "Gold: " + gold.ToString ();
		RemoveButtons ();
		AddButtons ();
	}

	private void RemoveButtons()
	{
		while (contentPanel.childCount > 0)
		{
			GameObject toRemove = transform.GetChild(0).gameObject;
			buttonObjectPool.ReturnObject(toRemove);
		}
	}

	private void AddButtons()
	{
        //Debug.Log("shop addButtons: " + shopList.Count);
        for (int i = 0; i < shopList.Count; i++)
		{
			ShopItem item = shopList[i];
			GameObject newButton = buttonObjectPool.GetObject();
			newButton.transform.SetParent(contentPanel);

			ShopButton sampleButton = newButton.GetComponent<ShopButton>();
			sampleButton.Setup(item, this);
            //Debug.Log("shop list: " + i);
        }
	}

	public void goDetail(ShopItem item){
		nameTag.text = item.names;
		priceTag.text = item.price.ToString ();
		pictTag.sprite = item.potos;
		descTag.text = item.desc;
	}

    public void activeList()
    {

    }

    public void buyItem(ShopItem item)
    {

    }
}
