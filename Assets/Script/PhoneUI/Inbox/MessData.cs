﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MessageItem
{
    public string name;
    public string city;
    //public Sprite profile;
    //public string profile_origin;
    public int dueDate;
    public int qty;
    public string orderType;
    public float price;

    public int status;
    //-1 not accepted
    //0 accept
    //1 sent
    //2 delivered
}

[System.Serializable]
public class MessDataStored
{
    public List<MessageItem> m_list, accept;

    public MessDataStored(MessData mess)
    {
        m_list = mess.all_list;
        accept = mess.acc_list;
    }
}

public class MessData : MonoBehaviour
//public class MessData : Singleton<MessData>
{
    public List<MessageItem> all_list, acc_list;

    public void addToList(MessageItem me, string opt)
    {
        if (opt == "acc")
            acc_list.Add(me);
        else if (opt == "all")
            all_list.Add(me);
    }

    public void removeFromList(MessageItem me, string opt)
    {
        if (opt == "acc")
            acc_list.Remove(me);
        else if (opt == "all")
            all_list.Remove(me);
    }

    public void updateList(List<MessageItem> li, string opt)
    {
        if (opt == "acc")
            acc_list = li ;
        else if (opt == "all")
            all_list = li;
    }

    public MessageItem getOne(string opt, int i)
    {
        if (opt == "acc")
            return acc_list[i];
        else if (opt == "all")
            return all_list[i];

        return all_list[i];
    }

    public List<MessageItem> getList(string opt)
    {
        if (opt == "acc")
            return acc_list;
        else if (opt == "all")
            return all_list;

        return all_list;
    }
}