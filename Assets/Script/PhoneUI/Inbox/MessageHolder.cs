﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageHolder : MonoBehaviour {

	public Text nameFront;
	public Image profilePict;
	//public Text cityFron;
	public Text dueDateFron;

	//public Text nameTag;
	//public Text cityTag;
	//public Text dueDateTag;
	//public Text qtyTag;
	//public Text orderTag;

	public Button button;

	private MessageItem mItem;
	private messageScrollList list;

	void Start(){
		button.onClick.AddListener (HandleClick);
	}

	public void Setup(MessageItem currentItem, messageScrollList currentList){

		Debug.Log("list: "+currentList);
		mItem = currentItem;
		//nameTag.text = mItem.name.ToString()+" - "+mItem.city.ToString();
		//profilePict.sprite = mItem.profile;
		//cityTag.text = mItem.city.ToString();
		//dueDateTag.text = temp_date;
		//qtyTag.text = temp_qty;
		//statusTag = 0;

		//dueDate = temp_date;
		//qty = temp_qty;

		nameFront.text = mItem.name.ToString()+" - "+mItem.city.ToString();
		//cityFron.text = mItem.city.ToString();
		dueDateFron.text = mItem.dueDate.ToString()+" Days";

		list = currentList;
	}

	public void HandleClick(){
		list.GoToDetail (mItem);
	}

}
