﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

//[System.Serializable]
//public class MessageItem
//{
//	public string name;
//	public string city;
//	public Sprite profile;
//	public int dueDate;
//	public int qty;
//	public string orderType;
//	public float price;
//}

//[System.Serializable]
//public class ListOfMessage
//{
//    public List<MessageItem> m_list;

//    ListOfMessage(messageScrollList mess)
//    {
//        m_list = mess.itemList;
//    }
//}

public class messageScrollList : MonoBehaviour {

    //public MessageItem m_dataItem;
    public MessData m_data;
	//public List<MessageItem> itemList, accList;
	public Transform contentPanel;
	public int time;
	//public ShopScrollList otherShop;
	//public Text myGoldDisplay;
	public SimpleObjectPool buttonObjectPool;

	public GameObject[] panel;
	public ScrollRect myScrollRect;
	public RectTransform[] scrollableContent;
	public int content_th, temp_content;

	public Text nameDes;
	public Text dueDateDes;
	public Text qtyDes;
	public Text OrderType;

	public MessageDB mDB;

    private MessageItem activeMess;

	void Start ()
	{
        m_data = GameObject.Find("GameLogs/MessageManager").gameObject.GetComponent<MessData>();
        time = 1;
		content_th = 0;
		temp_content = 0;

		scrollableContent [0].gameObject.SetActive (true);
		myScrollRect.content = scrollableContent [0];

        //m_data.Instance;
		RefreshDisplay ();
        //Debug.Log("number of mesage: " + m_data.getList("all").Count);
    }

	void Update (){
        if(m_data.getList("all").Count < 5)
        //if(itemList.Count < 5)
            time++;
		//if (time % 20 == 0 && itemList.Count < 5) {
        if (time % 20 == 0 && m_data.getList("all").Count < 5) {
			//Debug.Log ("now Time: " + time);
			GenerateNew ();
			//RefreshDisplay ();
		}
	}

	public void RefreshDisplay()
	{
		RemoveButtons ();
		AddButtons ();
	}

	private void RemoveButtons()
	{
        //Debug.Log("Content child: " + contentPanel.childCount);
        while (contentPanel.childCount > 0)
        {
            GameObject toRemove = contentPanel.GetChild(0).gameObject;
            buttonObjectPool.ReturnObject(toRemove);
        }
    }

	private void AddButtons()
	{
        int num = m_data.getList("all").Count;
		for (int i = 0; i < num; i++)
		{
            //MessageItem item = itemList[i];
            MessageItem item = m_data.getOne("all", i);
            GameObject newButton = buttonObjectPool.GetObject();
			newButton.transform.SetParent(contentPanel);

			MessageHolder sampleButton = newButton.GetComponent<MessageHolder>();
			sampleButton.Setup(item, this);
		}
	}

	public void GoToDetail(MessageItem mItem){
		content_th = 1;

		scrollableContent [content_th].gameObject.SetActive (true);
		myScrollRect.content = scrollableContent [content_th];

		Debug.Log (mItem.name + "-" + mItem.city + "order: "+mItem.orderType+ "- due date: " + mItem.dueDate + "quantity: -" + mItem.qty + "-" + mItem.price);
		nameDes.text = mItem.name.ToString () + " - " + mItem.city.ToString ();
		//cityDes.text = mItem.city.ToString ();
		qtyDes.text = mItem.qty.ToString ();
		OrderType.text = mItem.orderType.ToString ();
		dueDateDes.text = mItem.dueDate.ToString()+" Days left";

		scrollableContent [temp_content].gameObject.SetActive (false);
		temp_content = content_th;

        activeMess = mItem;
	}

	public void goBack(){
		if(content_th != 0){
            content_th = 0;

            scrollableContent [content_th].gameObject.SetActive (true);
			myScrollRect.content = scrollableContent [content_th];
			scrollableContent [temp_content].gameObject.SetActive (false);
			temp_content = content_th;
		}
	}

	public void GenerateNew(){

		string name = mDB.names[Random.Range(0, mDB.names.Length)];
		string city = mDB.city[Random.Range(0, mDB.city.Length)];
		string order = mDB.order [Random.Range (0, mDB.order.Length)];
		//Sprite profile = mDB.potos[Random.Range(0, mDB.potos.Length)];
		int dueDate = Random.Range(1,5);
		int qty = Random.Range(1,3);
		float price = Random.Range(1000,50000);

		//Debug.Log (name + "-" + city + "order: "+order+ "- due date: " + dueDate + "quantity: -" + qty + "-" + price);

		MessageItem new_Item = new MessageItem ();

		new_Item.name = name.ToString();
		new_Item.city = city.ToString();
		new_Item.orderType = order.ToString ();
		//new_Item.profile = profile;
		new_Item.dueDate = dueDate;
		new_Item.qty = qty;
		new_Item.price = price;
        new_Item.status = -1;

        addMessageObj(new_Item);
	}

    public void addMessageObj(MessageItem meItem)
    {
        GameObject newButton = buttonObjectPool.GetObject();
        newButton.transform.SetParent(contentPanel);

        MessageHolder sampleButton = newButton.GetComponent<MessageHolder>();
        sampleButton.Setup(meItem, this);

        m_data.addToList(meItem, "all");
        //itemList.Add(meItem);
    }

    public void acceptOrder()
    {
        //accList.Add(activeMess);
        activeMess.status = 0;
        m_data.addToList(activeMess, "acc");
        m_data.removeFromList(activeMess, "all");
        //itemList.Remove(activeMess);
        goBack();
        RefreshDisplay();
    }

    public void declineOrder() {
        //itemList.Remove(activeMess);
        m_data.removeFromList(activeMess, "all");
        goBack();
        RefreshDisplay();
        //RemoveButtons();
        //Invoke("RefreshDisplay", 0.5f);
    }
}
