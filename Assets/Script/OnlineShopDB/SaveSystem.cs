﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.IO;

public static class SaveSystem {

    public static void SaveOlShopList(ListOfItem ol)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + "saveOlshop.fun";

        FileStream stream = new FileStream(path, FileMode.Create);

        //ListOfItem data = new ListOfItem(ol);
        ListItemToSave data = new ListItemToSave(ol);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static ListItemToSave loadItem()
    {
        string path = Application.persistentDataPath + "/" + "saveOlshop.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            ListItemToSave data = formatter.Deserialize(stream) as ListItemToSave;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in: " + path);
            return null;
        }
    }

    public static void SaveMesList(MessData ss)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + "saveMess.fun";

        FileStream stream = new FileStream(path, FileMode.Create);

        //ListOfItem data = new ListOfItem(ol);
        MessDataStored data = new MessDataStored(ss);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static MessDataStored loadMessage()
    {
        string path = Application.persistentDataPath + "/" + "saveMess.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            MessDataStored data = formatter.Deserialize(stream) as MessDataStored;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in: " + path);
            return null;
        }
    }
}
