﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DBOlShopItem
{
    [SerializeField]
    public string spriteLoc;
    public string name;
    public float price;
    public int qty;
    public int category;
}

[System.Serializable]
public class ListItemToSave{
    public List<DBOlShopItem> l_list;
    public ListItemToSave(ListOfItem item)
    {
        l_list = item.list;
    }
}

public class ListOfItem : MonoBehaviour
{
    public List<DBOlShopItem> list;
    private DBOlShopItem one_item;
    //private ListItemToSave m_list;

    //public ListOfItem(ListOfItem item)
    //{
    //    list = item.list;
    //} 

    public void saveToFile()
    {
        SaveSystem.SaveOlShopList(this);
    }

    public void loadFromFile()
    {
        ListItemToSave data = SaveSystem.loadItem();

        list = data.l_list;
    }

    public void addList(string sp, string name, float price, int qty, int cat)
    {
        DBOlShopItem one_item = new DBOlShopItem();
        one_item.name = name;
        one_item.price = price;
        one_item.qty = qty;
        one_item.category = cat;
        one_item.spriteLoc = sp;
        list.Add(one_item);
    }
}
