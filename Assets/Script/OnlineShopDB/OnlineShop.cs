﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[System.Serializable]
//public class LocalOlShopItem
//{
//    Sprite sp;
//    string name;
//    float price;
//    int qty;
//    string category;
//}

[System.Serializable]
public class OnlineShop : MonoBehaviour
{
    private string[] fileImage = null;
    public Sprite[] spImg;
    public int imgIndex;

    public ListOfItem m_listOfItem;
    public InputField name, price, quantity;
    public Dropdown dd;
    public Image prev;

    void Start()
    {
        RetImg();
    }

    public void RetImg()
    {
        fileImage = Directory.GetFiles(Application.persistentDataPath + "/", "*.png");
        spImg = new Sprite[fileImage.Length];
        for(int i=0; i<fileImage.Length; i++)
        {
            string pathToFile = fileImage[i];

            Texture2D texture = GetScreenshotImage(pathToFile);
            spImg[i] = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
                new Vector2(0.5f, 0.5f));
        }
        if (fileImage.Length > 1)
            prev.sprite = spImg[0];
    }

    public void nextImg()
    {
        if(imgIndex > fileImage.Length)
        {
            imgIndex = 0;
        }
        else
        {
            imgIndex++;
        }

        changeImg();
    }

    public void prevImg()
    {
        if (imgIndex < 0)
        {
            imgIndex = fileImage.Length;
        }
        else
        {
            imgIndex--;
        }

        changeImg();
    }

    public void changeImg()
    {
        prev.sprite = spImg[imgIndex];
    }

    public void uploadData()
    {
        //m_listOfItem.addList(prev.sprite, name.text, float.Parse(price.text), int.Parse(quantity.text), dd.value);
        m_listOfItem.addList(fileImage[imgIndex], name.text, float.Parse(price.text), int.Parse(quantity.text), dd.value);
    }

    Texture2D GetScreenshotImage(string filePath)
    {
        Texture2D texture = null;
        byte[] fileBytes;
        if (File.Exists(filePath))
        {
            fileBytes = File.ReadAllBytes(filePath);
            texture = new Texture2D(2, 2, TextureFormat.RGB24, false);
            texture.LoadImage(fileBytes);
        }
        return texture;
    }
}
