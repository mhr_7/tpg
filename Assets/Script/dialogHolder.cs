﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogHolder : MonoBehaviour {

	public string[] dialogue;
	public string dialogue_type;
	public int[] option_place;
	private DialogueMan dMan;

	// Use this for initialization
	void Start () {
		dMan = FindObjectOfType<DialogueMan> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay2D(Collider2D co){
		if (co.gameObject.tag == "Player") {
			if (Input.GetKeyDown (KeyCode.Space)) {

				if (!dMan.dialogActive) {
					dMan.dialogue_type = dialogue_type;
					dMan.dialoguesList = dialogue;
					dMan.option_places = option_place;
					dMan.curr = -1;
					dMan.ShowBox ();
				}

				/*
				print("masuk space");
				if (!dMan.dialogActive) {
					dMan.ShowBox ();
				}
				print("masuk Show");
				dMan.write (dialogue [i]);
				print("sudah nulis");
				dMan.setRem (dialogue.Length-i);
				print("sudah set");
				i++;
				print("sudah increment");

				if (i == dialogue.Length) {
					i = 0;
				}
				*/
			}
		}
	}
}
