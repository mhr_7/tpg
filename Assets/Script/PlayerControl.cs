﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

	private Animator anim;
	public float input_x;
	public float input_y;
	public float speed;
	public bool isMovable;
	public static bool playerExist;

	// Use this for initialization
	void Start () {
		isMovable = true;
		//speed = 10000;
		anim = GetComponent<Animator> ();

		if (playerExist) {
			Destroy (gameObject);
		} else {
			playerExist = true;
			//DontDestroyOnLoad (transform.gameObject);
		}
	}

	// Update is called once per frame
	void Update () {

		input_x = Input.GetAxisRaw ("Horizontal");
		input_y = Input.GetAxisRaw ("Vertical");

		bool isWalk = (Mathf.Abs (input_x) + Mathf.Abs (input_y))>0;

		anim.SetBool ("isWalking", isWalk);
		if(isWalk && isMovable){
			anim.SetFloat("x", input_x);
			anim.SetFloat("y", input_y);

			transform.position += new Vector3(input_x*speed, input_y*speed, 0) * Time.deltaTime;
		}
	}
}
