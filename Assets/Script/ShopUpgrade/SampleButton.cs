﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SampleButton : MonoBehaviour {

	public Button button;
	public Text nameTag;
	public Text priceLag;
	public Image inconImage;

	private Item item;
	private ShopScrollList scrollList;

	// Use this for initialization
	void Start ()
	{
			button.onClick.AddListener (HandleClick);
	}

	public void Setup(Item currentItem, ShopScrollList currentScrollList)
	{
			item = currentItem;
			nameTag.text = item.itemName;
			inconImage.sprite = item.icon;
			priceLag.text = "Price: "+item.price.ToString ();
			scrollList = currentScrollList;

	}

	public void HandleClick()
	{
			scrollList.TryTransferItemToOtherShop (item);
	}
}
