﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FashionItem : MonoBehaviour {

	public string ItemName;
	public int ItemID;

	public Sprite unBroughtSprite;
	public Sprite BroughtSprite;

	public float price;
	public bool bought;
}
