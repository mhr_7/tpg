﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class new_PlayerCtrl : MonoBehaviour {

		private Animator anim;
		public float input_x;
		public float input_y;
		public float speed;
		public bool isMovable, facing_Right, facing_now;
		public static bool playerExist;

		// Use this for initialization
		void Start () {
			isMovable = true;
			anim = GetComponent<Animator> ();

			if (playerExist) {
				Destroy (gameObject);
			} else {
				playerExist = true;
				//DontDestroyOnLoad (transform.gameObject);
			}

		facing_Right = false;
		facing_now = false;
		}

		// Update is called once per frame
		void Update () {

			input_x = Input.GetAxisRaw ("Horizontal");
			input_y = Input.GetAxisRaw ("Vertical");

			bool isWalk = (Mathf.Abs (input_x) + Mathf.Abs (input_y))>0;

		if (input_x > 0) {
			facing_Right = true;
		} else {
			facing_Right = false;
		}

		if (facing_now != facing_Right) {
			Flip ();
			facing_now = facing_Right;
		}

			anim.SetBool ("isWalking", isWalk);
			if(isWalk && isMovable){
				transform.position += new Vector3(input_x*speed, input_y*speed, 0) * Time.deltaTime;
			}
		}

	void Flip(){
		float x_scale = transform.localScale.x * -1f;
		Vector3 temp = new Vector3 (x_scale, transform.localScale.y, transform.localScale.z);
		this.transform.localScale = temp;
	}
}
