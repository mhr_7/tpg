﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FashionShop : MonoBehaviour {

	public static FashionShop fashionShop;

	public List<FashionItem> fashion_list = new List<FashionItem> ();

	public GameObject itemPrefab;
	public Transform grid;

	// Use this for initialization
	void Start () {
		fashionShop = this;
		FillList ();
	}

	void FillList(){
		for (int i = 0; i < fashion_list.Count; i++) {
			GameObject holder = Instantiate (itemPrefab, grid);
			Shop_Item_Holder holderScript = holder.GetComponent<Shop_Item_Holder> ();

			holderScript.itemName.text = fashion_list [i].ItemName;
			holderScript.itemPrice.text = "Cr. "+fashion_list [i].price.ToString("N2");
			holderScript.itemID = fashion_list [i].ItemID;

			if (fashion_list [i].bought == true) {
				holderScript.itemImage.sprite = fashion_list [i].BroughtSprite;
			} else {
				holderScript.itemImage.sprite = fashion_list [i].unBroughtSprite;
			}
		}
	}
}
