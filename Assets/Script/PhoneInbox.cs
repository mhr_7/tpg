﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneInbox : MonoBehaviour {

	public GameObject[] panel;
	public ScrollRect myScrollRect;
	public RectTransform[] scrollableContent;
	public int content_th, temp_content;

	// Use this for initialization
	void Start () {
		content_th = 0;
		temp_content = 0;

		scrollableContent [0].gameObject.SetActive (true);
		myScrollRect.content = scrollableContent [0];
	}
	
	// Update is called once per frame
	void Update () {
		if (content_th != temp_content) {
			scrollableContent [content_th].gameObject.SetActive (true);
			myScrollRect.content = scrollableContent [content_th];
			scrollableContent [temp_content].gameObject.SetActive (false);
			temp_content = content_th;
		}
	}

	public void gotoDescription(int id){
		content_th = 1;
	}

	public void goBack(){
		content_th = 0;
	}
}
