﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DialogueMan : MonoBehaviour {

	public GameObject dBox, dOpt;
	public string dialogue_type;
	public Text dText;
	public int option = 0, curr = 0, opt_i = 0;
	public int[] option_places;
	public string[] dialoguesList;
	private PlayerControl pl;

	//public string[] dialogues;

	public bool dialogActive;
	// Use this for initialization
	void Start () {
        option = 0;
        curr = 0;
        opt_i = 0;
		pl = FindObjectOfType<PlayerControl> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (dialogActive && Input.GetKeyUp (KeyCode.Space)) {
			HideOpt ();
			curr++;
			//pl.isMovable = false;

			if (curr == option_places[opt_i] && dialogue_type == "option") {
                print("showing option");
				ShowOpt ();
			}

		}
		if (curr >= dialoguesList.Length) {
			dBox.SetActive (false);
			dialogActive = false;

			//pl.isMovable = true;
			curr = 0;
		}
		if (dialoguesList [curr] != null) {
			dText.text = dialoguesList [curr];
		}
	}

	public void ShowBox(){
		dialogActive = true;
		dBox.SetActive (true);
	}

	public void ShowOpt(){
		dOpt.SetActive (true);
	}
	public void HideOpt(){
		dOpt.SetActive (false);
	}
	public void getOptionYes(){
		option = 1;
		Debug.Log ("Button Yes clicked");
		curr++;
		opt_i++;
		HideOpt ();
	}		
	public void getOptionNo(){
		option = 0;
		Debug.Log ("Button No clicked");
		curr++;
		opt_i++;
		HideOpt ();
	}		
}
