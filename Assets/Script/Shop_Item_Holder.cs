﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop_Item_Holder : MonoBehaviour {

	public int itemID;
	public Text itemName;
	public Text itemPrice;
	public Image itemImage;
	public GameObject buyButton;
}
