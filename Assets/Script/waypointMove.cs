﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waypointMove : MonoBehaviour {

	public float speed, pos_x, pos_y;
	public Transform[] waypoints;
	public Vector2 temp_click, newest_click;
	public int i_next, i_now;
	// Use this for initialization
	void Start () {
		i_next = 1;
		i_now = 0;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetMouseButtonDown (0)) {
			pos_x = Input.mousePosition.x;
			pos_y = Input.mousePosition.y;

			temp_click = new Vector2 (pos_x, pos_y);
			newest_click = temp_click;
			//Debug.Log ("asdasdad %f, ashdbadj%f", pos_x, pos_y);
			//Debug.Log ("pos x - %f", pos_x);
			//Debug.Log ("pos y - %f", pos_y);
		}

		if (this.transform.position.x != newest_click.x && this.transform.position.y != newest_click.y) {
			move (this.transform.position, newest_click);
		}
	}

	void move(Vector2 now, Vector2 new_pos){
		this.transform.position = Vector2.MoveTowards (now, new_pos, speed * Time.deltaTime);
	}
}
