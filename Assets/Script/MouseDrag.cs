﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDrag : MonoBehaviour {

	public float speed;
	public float maxX, minX, maxY, minY;

	private RaycastHit mRayHit;
	private Ray mRay;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton (0)) {
			Vector3 pos = transform.position;
			//float xVal = Input.GetAxis ("Mouse X") * speed * Time.deltaTime;
			//Vector3 temp = new Vector3 (Input.GetAxis ("Mouse X") * speed * Time.deltaTime, Input.GetAxis ("Mouse Y") * speed * Time.deltaTime,0);
			//this.transform.position -= temp;
			if ((pos.x -= Input.GetAxis ("Mouse X")) >= minX && (pos.x -= Input.GetAxis ("Mouse X")) <= maxX &&
				(pos.y -= Input.GetAxis("Mouse Y")) >= minY && (pos.y -= Input.GetAxis("Mouse Y")) <= maxX) {
				pos.x -= Input.GetAxis ("Mouse X") * speed * Time.deltaTime;
				pos.y -= Input.GetAxis ("Mouse Y") * speed * Time.deltaTime;
				//float temp = Input.GetAxis("Mouse X");
				//Debug.Log("this is input "+temp);
				//print("hold down");
				transform.position = pos;

			}
		}
	}
}
