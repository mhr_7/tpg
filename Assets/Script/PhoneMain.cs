﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PhoneMain : MonoBehaviour {

	public GameObject[] panel;
    //public 
    public int panel_th, temp_panel;
    public float loadingDur;

	// Use this for initialization
	void Start () {
		panel_th = 0;
		temp_panel = 0;

		panel[0].gameObject.SetActive (true);
        panel[1].gameObject.SetActive(true);
        panel[2].gameObject.SetActive(true);

        StartCoroutine(loadingScreen());
    }
	
	// Update is called once per frame
	void Update () {
		if (panel_th != temp_panel) {
			panel [panel_th].gameObject.SetActive (true);
			panel [temp_panel].gameObject.SetActive(false);
			temp_panel = panel_th;
		}
	}

	public void gotoUpload(){
		panel_th = 2;
	}

	public void gotoInbox(){
		panel_th = 0;
	}

	public void gotoShop(){
		panel_th = 1;
	}

	public void gotoMain(){
		panel_th = 0;
	}

    public void backToRealGame()
    {
        SceneManager.LoadScene("room_med", LoadSceneMode.Single);
    }

    IEnumerator loadingScreen()
    {
        yield return new WaitForSeconds(loadingDur);

        panel[0].gameObject.SetActive(false);
        panel[1].gameObject.SetActive(false);
        panel[2].gameObject.SetActive(false);

        gotoMain();
    }
}
